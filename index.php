<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Accessible Slider</title>
	<link href="../style.css" rel="stylesheet">
</head>
<body>
<div id="c" class="carousel">
	<ul>
		<li class="slide">
			<img src="https://source.unsplash.com/user/erondu/1200x600" alt="This is the first image." />
		</li>
		<li class="slide">
			<img src="https://source.unsplash.com/user/timmossholder/1200x600" alt="This is the second image." />
		</li>
		<li class="slide">
			<img src="https://source.unsplash.com/user/janke/1200x600" alt="This is the third image." />
		</li>
	</ul>
</div>
<script src="../slider.js"></script>

</body>
</html>